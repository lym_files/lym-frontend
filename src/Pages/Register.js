import React, {useState} from 'react';
import {Button} from 'react-bootstrap'
import Swal from 'sweetalert2';

const Register = () => {

  
const handleSubmit = (e) => {
  e.preventDefault();

if (name !== "" && age !== "" &&  email !== "")
{
  fetch(`${process.env.REACT_APP_API_URL}/users/addUser`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      Name:    name,
      Age:     parseInt(age),
      Email:   email
  
       })
  })
  .then(response => response.json())
  .then(data => {
  if (data)   {
    Swal.fire({
      title: `${email} has been succesfully Registered`,
      html: 'You have successfully Registered',
      icon: 'success',
      confirmButtonText: 'Login',
      showCloseButton: true,
      customClass: {
        title: 'swal2-title',
        htmlContainer: 'swal2-html-container',
        confirmButton: 'swal2-confirm-button',
        closeButton: 'swal2-close-button' }
    })
    setEmail("")
    setAge("")
    setName("")

  } 
  else
  {
    Swal.fire({
      title: `Registration Failed`,
      html: 'Failed Registration',
      icon: 'failed',
      confirmButtonText: 'Register',
      confirmButton: 'ok'
      })}
      setEmail("")
      setAge("")
      setName("")
  })
  .catch(error => {
  if (error) {
    Swal.fire({
      title: `Registration Failed`,
      html: 'You have successfully Registered',
      icon: 'error',
      confirmButtonText: 'Register',
      confirmButton: 'ok'
      })
    }
    else {
      console.log("Test")
    }
     
  })

}
else {
  
  Swal.fire({
    title: `Invalid Input`,
    html: 'Failed Registration',
    icon: 'error',
    confirmButtonText: 'Register',
    confirmButton: 'ok'
    })
  }
  setEmail("")
  setAge("")
  setName("")
}

const errorMessage = () => {
  console.log("TEST")
}


  const [email, setEmail] = useState("")
  const [age, setAge] = useState("")
  const [name, setName] = useState("")

  console.log(email)
  console.log(typeof age)

  return (
    
    <div id='userRegister'>

      <div>
        <h1 className='headRegister'>REGISTER</h1>
        <form onSubmit={handleSubmit}>

          <label className='username'>Email
            <input className="inputRegister"  type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
          </label>

          <label className='username'>Name
            <input className="inputRegister"  type="text" value={name} onChange={(e) => setName(e.target.value)} />
          </label>

          <label className='username'>Age
            <input className="inputRegister"  type="number" value={age} onChange={(e) => setAge(e.target.value)} />
          </label>

          <Button className="buttonReg" variant="primary" type="submit">Register</Button>
        </form>
        {errorMessage && <p>{errorMessage}</p>}
      </div>
    </div>
  )
}

export default Register;
