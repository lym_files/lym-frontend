import React from 'react';
import { useState } from 'react';
import { Button } from 'react-bootstrap';



const errorMessage = () => {
   console.log(`Test`)

}

const handleSubmit = (e) => {
   console.log(`Test`)
}



const Login = () => {


   const [username, setUsername] = useState('');


   return (

      
      <div id='userRegister'>
        <div>
          <h1 className='headRegister'>Login</h1>
          <form onSubmit={handleSubmit}>
            <label className='username'>
              <input className="inputRegister" type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
            </label>
             <p className='user'>Username</p>
            <Button className="buttonReg" variant="primary" type="submit">Login</Button>
          </form>
          {errorMessage && <p>{errorMessage}</p>}
        </div>
      </div>
    
    
    
      )
};


export default Login;
